---
title: "Resolvez vos problèmes d'internet"
description: "Comment pousser votre FAI et leurs techniciens à résoudres vos problèmes de connexion internet quand on quelque chose cloche ?"
publishDate: "01 May 2024"
# updatedDate: "09 April 2024"
tags: ["internet", "guide", "réseaux"]
---


## Où nous nous sommes laissés
J'étais jeune et des hypothèses plein la tête, mais le coup de sang passé je me suis rappelé que je n'étais pas aussi intéressé que ça pour chercher comment retrouver les adresses spécifiques par protocole que me renvoient les sites qui acceptent nos mini DDoS.

En plus, en cherchant un meilleur moyen d'avoir un CMS correct pour le site vu la manière dont je "travaille" et que je prend déjà des notes au bon format sur mon ordi mais en même temps pas vraiment (une longue histoire pour un autre article), je me suis rendu compte des limitations que logseq m'imposait personnellement et j'ai commencé à me faire draguer lourdement par emacs.

Enfin je voulais surtout une solution rapide parce que c'est marrant deux minutes mais je n'ai pas toutes les clés en main pour mettre fin à ma frustration non plus...

## Diagnostic d'une boite noire
Et c'était là le problème principal. Je vois ce que les techniciens font devant moi mais pas les actions prises au niveau du plateau technique[^1]. Les pousser à la bonne action le vrai challenge donc on change de stratégie et on revient aux bases, si l'IPV6 ne marche pas et malgré ma connexion précédemment catastrophique sans, on va demander à se faire installer une ancienne box qui ne le supporte pas[^2] juste pour avoir un vrai paramétrage qui ne se concentre pas sur la technologie à déconnexions perpétuelles.

[^1]: J'ai posé la question au support après un appel pour vérifier ma satisfaction qui m'a confirmé celà. Mais ce n'était pas un membre du personnel technique donc ça vaut ce que ça vaut.

[^2]: Parce que d'après le support encore une fois, impossible de désactiver l'IPV6 sur les nouveaux modèles de routers (l'option existe pourtant).

Au pire je réussi à les refaire configurer et on accompli le miracle ensemble (jamais 7 sans 8 comme ils disent) même si j'y crois qu'à moitié, au mieux je repasse sur la tech maîtrisée en leur faisant apporter une solution à mon problème précédent et on se dit au revoir en bons termes. Pas de proxy, pas de représentation en mon absence, on attend patiemment le rendez-vous mais on travaille ensemble maintenant...

## L'important c'est de gagner le loto au technicien
Des personnes très agréables au téléphone, test de l'installation et de la connexion avant de procéder au changement demandé. Suite à mes explications et console mise à côté de la box lépreuse pour éviter toute véléhité de négation de la réalité douloureuse qu'est ma connectivité internet, ils m'invitent à procéder aux tests en leur présence sur la nouvelle box histoire de savoir si ça résoud bien mon problème[^3]. Pas moi en train de faire mon petit stream local genshin impact pour montrer le ping instable et stratosphérique (it's over 999ms) mais sans déconnexion sèche. Et ça prend des photos en plus pour le rapport et envoyer au supérieur, je ne vous cache pas que la petite larme a failli couler après tout ce que j'ai traversé.

[^3]: Vous savez faire ça ? Je n'y croyais plus.

La config IPV4 ne pouvant pas être meilleure, je leur demande de remettre l'autre routeur vu que notre problème reste complet. Ils rappelle pour qu'on me refasse une configuration premium++ pour clôturer le service de roi qu'ils m'offrent cette soirée. On refait un test après installation et les mesures ont l'air correctes mais je ne me fais pas trop d'espoir. Ils me promettent d'envoyer tous les éléments et me demandent de ne pas hésiter à leur partager les enregistrements si le ping en jeu fait encore des montagnes russes (Sony quels rois quand même de permettre d'enregistrer jusqu'à la dernière minute de gameplay). On se dit au revoir tout sourire, mes pensées déjà tournées vers la prochaine étape de ma bataille si je n'ai pas de résultat et console éteinte parce que je n'en peut plus de ce jeu en fait même s'il sert de bon témoin.

## Enfin sauvé... Mais pas au bout de mes peines
Grosse surprise le lendemain quand je reçois un appel du service client pour savoir si j'ai satisfaction après le passage des techniciens[^3]. On relance Genshin quand on rentre pour effectuer un test comme promis et... Tout est clean ? Un ping constant comme Jésus, petite tentation de 30m dans le désert mais c'était la connexion qui était catastrophique sinon RAS. Je n'en crois pas mes yeux, il a fallu 7 rapports fermés quand le coeur leur en dit pour que je sois enfin bien traité.

Mais pourquoi j'ai crié encore sur twitter ? Parce qu'un soir où je me suis connecté plus tard que d'habitude ils ont encore frappé. Des sites lents comme la mort qui me font tester la connexion avec le service de cloudflare pour découvrir 1s minimum de ping même en téléchargement et le tiers du débit auquel j'ai souscris. Comme d'habitude le SAV téléphonique qui ne m'est d'aucune aide, n'a visiblement pas ouvert de dérangement pour moi non plus mais heureusement j'ai aboyé sur internet pour être pris en charge. Problème temporaire ou intervention musclée de leur côté, ça ne m'intéresse pas trop en vrai mais pour moi c'était une soirée de travail perdue, surtout moralement parce que ça faisait exactement une semaine de répit.

## Conclusion
On en où aujourd'hui ? Près d'une semaine après la grosse frayeur qui m'a envoyé au lit, pas de soucis en vue mais on reste sur le qui-vive puisque le service ne sait pas se tenir les quelques heures où je l'utilise en semaine.
- comme dit précédemment, le technicien fait l'intervention si ton problème ne peut pas être réglé avec l'assistance du SAV au téléphone
- ils sont incapables de voir quand je ne reçois pas le débit que je devrais donc on maintient la politique de l'appel au premier hoquet (j'ai assez testé pour voir les valeurs quand les performances sont correctes. Je ne suis pas une peste non plus)
- la 8e configuration était la bonne. Parce que j'ai fait reconfigurer un autre type de routeur avant ou parce que le 7 précédents n'étaient pas bon ? Ou même une IP côté FAI n'était pas bonne ? On ne le saura jamais. Pas plus que je ne comprend ce qu'ils testent pour penser que je reçois ce à quoi je souscris comme débit quand ce n'est visiblement pas le cas mais ce n'est plus mon problème maintenant. Cependant si un sachant veut m'éclairer, je suis preneur.
- Les pings mesurés en IPV6 sont plus élevés qu'en IPV4 mais il en résulte le même service au final. Les mesures ne peuvent pas être prises sans considérer la technologie utilisée, sinon on compare de pommes à des oranges.
- J'ai enfin désinstallé Genshin parce que c'en est assez.
