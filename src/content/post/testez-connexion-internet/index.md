---
title: "Testez votre connexion internet"
description: "Comment obtenir et interpréter des chiffres sur la performances de sa connexion internet quand on suspecte que quelque chose cloche ?"
publishDate: "07 April 2024"
updatedDate: "09 April 2024"
tags: ["internet", "guide", "réseaux"]
---

Deux semaines de perturbations, des lenteurs en streaming youtube et netflix, des déconnexions sèches des serveurs de Genshin (imaginez si même moi je dois m'en plaindre)... Tu soupçonnes que quelque cloche avec ta connexion mais ton FAI te prend pour un clown ? Voici le mode d'emploi.

## Objectif et hypothèse
La description de ma plainte n'est peut-être pas assez précise pour les techniciens professionnels qui ne se sont pas foulés de faire un diagnostic eux-même. Enfin je pense, s'ils sont capable de réinitialiser ma box et configurer l'ipv6 à tour de rôle et partir en considérant leur travail effectué. Pour prendre les choses en main je veux savoir ce qu'il se passe avec ma connexion au moment où Genshin me considère déconnecté. Pour rappel j'ai la fibre et des gens jouent à Genshin avec leur connexion mobile 4G.

---
**Hypothèse**: Si j'ai des micro déconnexions des serveurs de Genshin (on ne parle même pas de LoL), alors je dois perdre des paquets autour de cette période.

## Quelques défintions
**FAI**: Fournisseur d'accès internet.

**Bande passante**: La fameuse super valeur que te vend ton FAI, mais que tu n'arrive jamais à atteindre en téléchargement. Elle est différente de la vitesse qui représente juste la vélocité d'arrivée sur ta machine alors que la bande passante est la capacité totale du réseaux. La somme des vitesses de téléchargement de tous tes appareils se rapproche plus de ta bande passante que celle d'un seul.

**Latence**: Le temps que prend un message pour aller au destinaire et te revenir.

## Regarder les paquets envoyés en temps réel avec l'utilitaire `ping`
On commence par le premier truc auquel je pense. Si je peux jouer à Genshin en même temps en regardant en live la performance de ma connexion, je pourrai observer les comportements des paquets.

La commande `ping` est un classique des utilitaires réseau linux qui sert à envoyer des *paquets* vers un serveur pendant un certain temps et observer le temps de réponse (aka *la latence*). Pour être sûr de ne pas tomber sur un serveur défaillant, j'ai effectué le test en parralèle sur 3 serveurs qui ont l'air d'accepter l'envoi de ping sauvages comme ici:
- google.com
- cnn.com (d'après divers forums)
- yahoo.com (ils doivent bien servir à quelque chose)

Décomposons la commande que j'ai utilisée:
```bash
ping -w 600 -DU -T tsonly google.com | tee logile.log
```
- `ping` envoie des paquets de 64 bits par défaut sur un serveur;
- `-w 600` pour que les pings s'arrêtent après 600 secondes;
- `-D` rajoute les timestamps (temps de mesure) à chaque ligne = temps unix + microseconds. On n'est jamais trop précis;
- `-U` pour avoir la latence de l'utilisateur vers l'utilisateur. Le chrono part de ta chambre plutôt que du portail de la maison;
- `-T tsonly` pour afficher les timestamp pour chaque enregistrement;
- `| tee logfile.log` pour afficher les résultats dans le terminal et en même temps les enregistrer dans le fichier avec la commande `tee`;

Je regarde sur les 4 serveurs en même temps mais rien de spécial parfois au moment où Genshin m'éjecte[^1]. J'ai quand même pu observer lors des premiers tests ceci:
![Pic de latence sur tous les serveurs au moment de la déconnexion](./ping-lag-spike.png)
Par contre très très flou toutes les latences à minimum 70ms pour de la fibre optique[^1].
Aussi tous les pics de latence ne coincident pas. Sur cette période pendant les 10 minutes de test le ping de yahoo.com était >200ms quand tous les autres étaient plus normaux.
Il y a des éléments qui manquent et si je n'arrive pas à trouver c'est quoi, normal qu'ils n'y arrivent pas non plus.

[^1]: Les logs d'une moment où j'ai eu une deconnexion de Genshin mais les serveurs semblaient normaux (yahoo était normalement malade).
![Latence élevée uniquement sur yahoo.com](./ping-yahoo-only-spike.png)

## Les sites de tests de débit, une valeur sûre
Après l'appel avec un technicien qui m'a mis hors de moi, j'ai compris quelques choses:
1. Ils se contentent de tests de débit pour décréter que la connexion est bonne ou pas
2. Ils ne savent pas utiliser les tests de débit ou ne regarde que le nombre une fois
3. Ils ne comprennent pas tous ce qu'un problème intermittent et intempestif veut dire

Mais je suis convaincu d'une chose, le test doit leur montrer une valeur anormale vu que les pics que j'ai vu lors du précédent test ne sont pas cantonnés aux moments de déconnexion (et j'ai enfin fait attention au ping montré par le jeu qui se met à danser la samba malgré le cable éthernet).

On applique le même mode opératoire et je trouve 3 sites qui peuvent faire l'affaire.
- https://speed.cloudflare.com/ le meilleur;
- www.speedtest.com le classique (utilisé par les techniciens) [^2];
- www.fast.com outil de netflix pour avoir des résultats variés;

[^2]: J'ai vérifié et le ping en téléversement est trop élevé ici aussi. Ils ne regardent donc juste pas.

On va se concentrer sur les résultats du meilleur tellement ils sont variés (et qui dit test plus long, dit plus de chance de capturer les anomalies). Voici les derniers résultats:
![résultats de la latence cloudflare](./speed-cloudflare.png)

Quelques éléments:
- Plusieurs tailles de données sont utilisées pour les tests de téléchargement ET de téléversement;
- On peut voir sur la boîte à moustache que la latence en téléversement va dans tous les sens. Au cours du test c'est le téléversement de 10MB qui dégrade le plus le graphe;
- Les résumés présentés en *Latency* et *Jitter* nous présentent les valeurs médianes et les variation de latence respectivement. Dans les deux cas, plus la valeur est faible, meilleure elle est;
- On est pas sur une campagne de dénigrement mais je vous laisse effectuer le test en 4G, c'est peut-être moi le problème on sait pas;
- **UPDATE** j'ai pu comparer avec leur 4G et je ne sais pas si je dois rire ou pleurer.

On voit que la latence peu reluisante en téléchargement, pour de la fibre je répète, est absolument risible en téléversement. La variation élevée est là pour ne rien arranger. Genshin Impact est un jeu mobile très permissif au niveau de la stabilité de la connexion, attendre plus de 2 secondes plusieurs messages, je comprend pourquoi malgré ma bande passante normale j'ai toujours des déconnexions.

## Conclusions
- Ce que je pensais être un problème intermittent était en fait une dégradation globale et dramatique de ma connexion internet et les déconnexions du serveur de Genshin dûes à des pics de latence plutôt que des paquets perdus (quoique techniquement il les considère comme tel)
- Soyez présent lors du passage du technicien, il est possiblement un meilleur interlocuteur que le service client du FAI. S'il comprend rien faut pas forcer non plus, attendez le prochain mais assurez vous qu'il n'oublie pas de paramétrer quelque chose avant son départ. Je suis tombé pour que vous puissiez marcher.
- Nous sommes à 6 ou 7 passages de technciens avec le palmarès de punchline suivant:[^3]
- Après quelque recherche, la migration vers l'IPV6 peut avoir de tels résultats (comprendre: catastrophiques) d'après des utilisateurs asiatiques. Peut-être demander à retourner sur de l'ipv4 en priorité. Une partie 2 à venir ?

[^3]: Masterclass sur masterclass:
	- "Ton débit est faible, tu devrais l'augmenter" alors qu'il devrait à minima s'assurer de me fournir 20MBPS de bande passante
	- 6 reparamétrage de box
	- 1 rapport qui se targue d'avoir résolu mon problème expliquant les 4 dérangements fermés chez l'opérateur
	- 1 technicien compétent qui comprend que quelque chose ne va pas 🥳
	- Un duo qui veut me gaslight en live et blâmer mes appareils alors que je fais les tests à côté d'eux
