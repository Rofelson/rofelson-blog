---
title: "Recap 2024"
author: ["rofelson"]
publishDate: 2025-01-19T00:00:00+00:00
updatedDate: 2025-01-20T21:54:28+00:00
tags: ["self"]
draft: false
description: "Un retour sur mon intense année 2024... Mais abrégé"
---

## Intro

Je me suis rendu compte que j'ai eu une seconde moitié de 2024 assez folle. Je viens ici en faire le bilan, moitié pour m'expliquer pourquoi je me suis perdu de vue, moitié pour prendre des engagements en public.


## Une fin d'année sur les chapeaux de roue

Académiquement, l'objectif était clair : en finir avec ce master en **Intelligence Artificielle/Data Engineering** (et il faut que j'aille flex chez le prof qui m'a dit que ça allait pas être simple de m'y remettre parce qu'il avait grave raison) et raccrocher le tablier pour passer le flambeau à la jeunesse. L'annonce des dates de soutenances qui correspondaient à celles de mes congés a tout goupillé, le projet **_#Liberté2025_** était lancé... et s'est bien terminé, enfin. Et parce que jouer en mode facile ne m'intéresse pas, je m'étais rajouté une quête secondaire de mentorat data assez chronophage. L'expérience fut fort intéressante et me servait accessoirement à justifier mon mode de vie de savant fou.

Niveau vie sociale, jusqu'à l'amorce du fameux projet **_#Liberté2025_**, ce n'était pas incroyable. Assez chanceux pour que les relations chères à mes yeux ne soient pas les plus compliquées à entretenir mais je n'avais pas beaucoup de temps pour tenir des engagements. Une profonde pensée à tous ces mariages manqués mais mon letterboxd peut attester de mon cruel manque de temps.

Un temps limité pour mes projets persos aussi. En dehors de ma guerre avec Orange qui m'a rendu très loquace ici, je n'étais pas très présent sur les réseaux en général. Pas assez de temps selon moi pour [PAL](https:www.pals.africa) mais l'avantage de ne pas travailler seul c'est que ça peut avancer même quand tu es en effort réduit.


## Mon palais personnel

Pas beaucoup de temps pour mes projets perso donc pas assez pour l'entretien de mon palais personnel, le système opérationnel de mon ordinateur. Délaisser un peu mon bureau historique pour les terres inconnues des _[tiling windows manager](https:https://wiki.archlinux.org/title/Comparison_of_tiling_window_managers)_ m'a lancé dans un chantier sans fin d'accessibilité. Un jour peut-être, la présentation de la distribution linux camouflée sous laquelle je tourne et tout le reste.
Ma folie des claviers mécaniques m'a aussi poussé naturellement vers l'achat d'un petit clavier ergonomique pour tester. Je ne l'ai pas beaucoup utilisé parce que ça me ralentissait pour taper du texte, pas très pratique pour un master où ça code... Mais c'est comme une douche pendant l'harmattan, il faut y aller franco une fois qu'on est décidé.

Pour la prise de notes, j'ai décidé de tester emacs (doom-emacs) pour org-mode et d'autres packages interessants, parce que je me trouvais limité par [logseq](https:https://logseq.com/) dont le contenu n'est pas assez exportables vers d'autres formats à mon goût. Je me heurtais aussi à plein de bugs quand les notes comportaient trop de noeuds ou de caractères mais le projet est en réécriture pour utiliser une base de donnée plutôt que les fichiers markdown a l'air prometteur, ça pourrait être intéressant au travail. Friendship with [Obsidian](https:https://obsidian.md/) never started.

Dernier point, ma recherche d'un package introuvable sur ma distro m'a fait m'intéresser au sujet de la maintenance de package pour une distribution/communauté. Je ne me suis jamais lancé faute de temps donc ceci est un rappel.


## Zyzz mania

Il m'intéresse pas du tout, mais entre mon corps qui me rappelle mon âge et que je suis finito, et la réalisation que je n'ai plus vraiment eu d'activité physique régulière depuis ma dernière année d'école d'ingé, je m'étais dit qu'il fallait faire quelque chose. Out la salle de sport pour l'instant parce que je connais déjà un peu, out la course à cette période parce que je n'aime pas l'idée de courir pour rien, out la salle de sport de combat parce mes horaires ~~et les tarifs~~. Je ne sais plus depuis quand j'ai la barre de traction mais je pense que c'est de l'année d'avant (volonté de l'utiliser soldée par un cuisant échec cette année encore), mais je me suis pris une bicy... un vélo qui a connu plus de succès. On va reprendre sur cette bonne lancée et remettre un peu de force et de mobilité en temps voulu.

Ah et enfin un bureau digne de ce nom avec un écran plus grand. Si si, ça participe je te promet. Bonne posture, toussa toussa.


## De l'année en cours ou la liberté nouvelle

Comme d'habitude, beaucoup trop de plans pour mes 24h quotidiennes mais je pense qu'on peut faire quelques trucs en étant respectueux de mon temps. On va commencer par plus d'activité sur mes plateformes, ici d'abord, les réseaux sociaux ensuite (et mes étoiles honteuses sur letterboxd) et youtube peut-être à nouveau qui sait. J'aimerais aussi faire quelques changements graphiques mais je dois attraper un certain gars pour ça... Nous sachons qui tu es, tu ne peux pas fuir indéfiniment.

Qui dit plus de temps libre donc plus de temps pour être présent humainement, sortir, rendre visite et appeler. Mais aussi pour tester des choses et faire des Proof Of Concepts (Je n'avais pas envie de l'écrire celui là mais nous sachons). Laquelle de ces deux forces antagonistes vaincra ? Suspens.

Au chapitre des tests il y a essayer le stream si Orange veut bien me le permettre mais bon on a jamais droit au bonheur trop longtemps avec eux, et aussi la reprise des vidéos (jeux) si j'arrive à suivre assez de trucs intéressants et libérer du temps pour me poser surtout parfois en rentrant du travail.
Et pour finir reprendre mes sorties cinés et ma balade physique du weekend ne serait pas mauvais non plus.
Utiliser mon temps de façon plus respectueuse de moi même, surtout maintenant que j'ai mes weekends, même si j'écris ces lignes à une heure où je devrais être en train de ronfler.
